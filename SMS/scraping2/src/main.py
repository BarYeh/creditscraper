import scrapy 
from scrapy_splash import SplashRequest 
class MySpider(scrapy.Spider): 
    start_urls = ["http://example.com", "http://example.com/foo"]
    name = 'blogspider'
    
    def start_requests(self): 
        for url in self.start_urls: 
            yield SplashRequest(url, self.parse, 
                endpoint='render.html', 
                args={'wait': 0.5}, 
           ) 
    def parse(self, response):
        print('parsing')

print('main was run')

from time import sleep

spidy = MySpider()
res = spidy.start_requests()
print(res)
for i in range(12000):
    sleep(1)
    print(i)